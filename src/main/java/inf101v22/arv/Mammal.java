package inf101v22.arv;

public abstract class Mammal {
    
    private String name;

    Mammal(String name) {
        this.name = name;
    }

    void sleep() throws DoesntSleepException {
        System.out.println("The " +getMammalType() + " " + this.name + " goes to sleep");
    }

    void speak() {
        System.out.println(this.name + " says:" + this.getNoise());
    }

    String getMammalType() {
        return this.getClass().getSimpleName().toLowerCase();
    }

    abstract String getNoise();
}

package inf101v22.arv;

public class Main {
	
	public static void main(String[] args) throws DoesntSleepException {
		Mammal a = new Cat("Angela");
		Mammal b = new BlackPanther("Boris");
		Cat c = new Cat("Xi");
		Owl o = new Owl("Joe");

		try {
			o.sleep();
		} catch (Exception e) {
			System.out.println("I a catch DoesntSleepException" + 
			" (as well as every other type of exception that" + 
			" ultimately extend the Exception class)");
		}
		a.sleep();
		b.sleep();
		c.sleep();
		a.speak();
		b.speak();
		c.speak();
	}
}
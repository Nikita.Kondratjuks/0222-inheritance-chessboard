package inf101v22.grafikk;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class Main {
    
    public static void main(String[] args) {
        JComponent view = new View();
        
        JFrame frame = new JFrame("INF101");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(view);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }
}
